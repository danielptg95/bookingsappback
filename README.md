# BookingsAppBack



## Daniel Paternina, 2024/06/20


El proyecto se encuentra desarrollado en angular15, se debe configurar la variable "baseURL" en el archivo environment.ts, para conectar con el proyecto back, por ejemplo:
```sh
export const environment = {
  production: true,
  baseURL:"https://localhost:7197"
};
```
